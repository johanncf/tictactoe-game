import java.util.concurrent.TimeUnit;

import mqtt.MQTTclient;
import runtime.Scheduler;
import tictactoe.GameControllerMachine;
import tictactoe.GameManager;

public class App {
    public static void main(String[] args) 
	{
        String broker = "tcp://broker.hivemq.com:1883"; // args[0];
		String topic = "Cell"; // args[1];
		String address = "SenseHatPiTTM4160";

		GameControllerMachine stm = new GameControllerMachine();
		Scheduler s = new Scheduler(stm);
        
		stm.createGameManager(s);

        MQTTclient mqttClient = new MQTTclient(broker, address, false, s);
		mqttClient.subscribeToTopic(topic);

		s.start();
		//try {
		//	// DEBUG :
		//	stm.fire("Button", s);
		//	stm.fire("Cell;0", s);  // P1
		//	TimeUnit.SECONDS.sleep(1);
		//	stm.fire("Cell;3", s);  // P2
		//	stm.fire("Cell;1", s);  // P1
		//	TimeUnit.SECONDS.sleep(1);
		//	stm.fire("Cell;4", s);  // P2
		//	stm.fire("Cell;2", s);  // P1
		//	stm.display();
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
		
//		GameManager game = new GameManager();
//		
//		game.addToken(1, 0);
//		game.addToken(1, 1);
//		game.addToken(2, 2);
//		game.addToken(2, 3);
//		game.addToken(1, 4);
//		game.addToken(2, 5);
//		game.addToken(1, 6);
//		game.addToken(2, 7);
//		game.addToken(1, 8);
//		
//		displayGrid(game);
//		
//		System.out.println(game.checkWin(1));
//		System.out.println(game.checkWin(2));
//		System.out.println(game.checkFull());
		
	}
	
	public static void displayGrid(GameManager game)
	{
		System.out.println(game.getGrid()[0] + " " + game.getGrid()[1] + " " + game.getGrid()[2]);
		System.out.println(game.getGrid()[3] + " " + game.getGrid()[4] + " " + game.getGrid()[5]);
		System.out.println(game.getGrid()[6] + " " + game.getGrid()[7] + " " + game.getGrid()[8]);
	}
}
