package sensehat;

import java.util.LinkedList;

import mqtt.MQTTclient;
import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;

public class SensehatControllerMachine implements IStateMachine {

	private static final String Timer_1 = "t1";

	private enum STATES {
		IDLE, DISPLAY, WAIT_FOR_MESSAGE
	}

	private Timer t1 = new Timer(Timer_1);

	protected STATES state = STATES.IDLE;

	private LEDMatrixTicker ticker;

	private LinkedList<String> textQueue = new LinkedList<String>();
	private Freepool freepool;

	public SensehatControllerMachine(int maxFreepools) {
		freepool = new Freepool(maxFreepools);
	}

	private String filterMQTTEvent(String event) {
		return event.substring(event.indexOf(';') + 1, event.length());
	}

	public void configureMQTTandTopic(MQTTclient mqttClient, String topic) {
		freepool.configureMQTTandTopic(mqttClient, topic);
	}

	public void createLedTicker(Scheduler s) {
		ticker = new LEDMatrixTicker(s);
	}

	public int fire(String event, Scheduler scheduler) {
		if (state == STATES.IDLE) {
			if (event.startsWith("MQTTMsg:freepool")) {
				freepool.receiveFreepool();
				state = STATES.IDLE;
				return EXECUTE_TRANSITION;
			} else if (event.startsWith("MQTTMsg:")) {
				// start writing
				ticker.StartWriting(filterMQTTEvent(event));
				state = STATES.DISPLAY;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.DISPLAY) {
			if (event.equals(Timer_1)) {
				// writing step
				ticker.WritingStep();
				state = STATES.DISPLAY;
				return EXECUTE_TRANSITION;
			} else if (event.equals("LEDMatrixTickerWait")) {
				// start timer
				t1.start(scheduler, 100);
				state = STATES.DISPLAY;
				return EXECUTE_TRANSITION;
			} else if (event.equals("LEDMatrixTickerFinished")) {
				freepool.sendFreepool();
				if (textQueue.isEmpty()) {
					state = STATES.IDLE;
				} else {
					ticker.StartWriting(textQueue.pollFirst());
					state = STATES.DISPLAY;
				}
				return EXECUTE_TRANSITION;
			} else if (event.startsWith("MQTTMsg:freepool")) {
				freepool.receiveFreepool();
				state = STATES.DISPLAY;
				return EXECUTE_TRANSITION;
			} else if (event.startsWith("MQTTMsg:")) {
				// start writing
				textQueue.add(filterMQTTEvent(event));
				state = STATES.DISPLAY;
				return EXECUTE_TRANSITION;
			}
		}
		return DISCARD_EVENT;
	}

	public static void main(String[] args) {
		String broker = "tcp://broker.hivemq.com:1883"; // args[0];
		String topic = "SenseHatText"; // args[1];
		String address = "SenseHatPiTTM4160_";
		SensehatControllerMachine stm = new SensehatControllerMachine(0);
		Scheduler s = new Scheduler(stm);

		MQTTclient mqttClient = new MQTTclient(broker, address, false, s);
		mqttClient.subscribeToTopic(topic);

		stm.configureMQTTandTopic(mqttClient, topic);

		stm.createLedTicker(s);

		s.start();
	}
}
