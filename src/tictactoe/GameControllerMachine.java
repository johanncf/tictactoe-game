package tictactoe;

import mqtt.MQTTclient;
import runtime.EventWindow;
import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;
import sensehat.Freepool;

public class GameControllerMachine implements IStateMachine {
	
	private static final String BUTTON_PRESSED = "Button";
	
	public static final String[] EVENTS = {BUTTON_PRESSED};
	
	private enum STATES {READY, P1, P2}
	
	private Timer t0 = new Timer("t0");
	
	protected STATES state = STATES.P1;

	private GameManager gameManager;
	
	public GameControllerMachine() {
		// Board initialization
	}

	public void createGameManager(Scheduler s) {
		gameManager= new GameManager(s);
	}

	private String filterMQTTEvent(String event) {
		return event.substring(event.indexOf(';') + 1, event.length());
	}
	
	public void display()
	{
		System.out.println(gameManager.getGrid()[0] + " " + gameManager.getGrid()[1] + " " + gameManager.getGrid()[2]);
		System.out.println(gameManager.getGrid()[3] + " " + gameManager.getGrid()[4] + " " + gameManager.getGrid()[5]);
		System.out.println(gameManager.getGrid()[6] + " " + gameManager.getGrid()[7] + " " + gameManager.getGrid()[8]);
	}

	public int fire(String event, Scheduler scheduler) {
		switch (state) {
		case READY:
			if(event.contains(BUTTON_PRESSED)) {
				// Clear the grid
				gameManager.resetGame();
				state = STATES.P1;
				return EXECUTE_TRANSITION;
			}
			break;

		case P1:
			// First iteration: User enters number from 1-9 in HiveMq
			// Add the token in the grid
			// If P1 won -> go to DISPLAY_RESULT (P1 won)
			// Else if the grid is full -> go to DISPLAY_RESULT (draw)
			// Else -> Go to P2
			
			if(event.contains("Cell;")){
				if (gameManager.addToken(1, Integer.parseInt(filterMQTTEvent(event)))) {
					state = STATES.P2;

					if(gameManager.checkWin(1)){
						state = STATES.READY;
						t0.start(scheduler, 0);
						System.out.println("P1 wins");
					}
					else if(gameManager.checkFull()){
						state = STATES.READY;
						t0.start(scheduler, 0);
						System.out.println("Draw");
					}

					return EXECUTE_TRANSITION;
				}
			}
			break;
			
		case P2:
			// First iteration: User enters number from 1-9 in HiveMq
			// Add the token in the grid
			// If P2 won -> go to DISPLAY_RESULT (P2 won)
			// Else if the grid is full -> go to DISPLAY_RESULT (draw)
			// Else -> Go to P1

			if(event.contains("Cell;")){
				if (gameManager.addToken(2, Integer.parseInt(filterMQTTEvent(event)))) {
					state = STATES.P1;

					if(gameManager.checkWin(2)){
						state = STATES.READY;
						t0.start(scheduler, 0);
						System.out.println("P2 wins");
					}
					else if(gameManager.checkFull()){
						state = STATES.READY;
						t0.start(scheduler, 0);
						System.out.println("Draw");
					}

					return EXECUTE_TRANSITION;
				}
			}
			break;

		default: break;
		}

		return DISCARD_EVENT;
	}
	
	public static void main(String[] args) {
		GameControllerMachine stm = new GameControllerMachine();
		Scheduler s = new Scheduler(stm);

		stm.createGameManager(s);

		s.start();
	}

}
