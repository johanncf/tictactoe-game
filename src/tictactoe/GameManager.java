package tictactoe;

import runtime.Scheduler;
import sensehat.LEDMatrixTicker;

public class GameManager {

    private LEDMatrixTicker ledMatrix;

    public GameManager(Scheduler s) {
        ledMatrix = new LEDMatrixTicker(s);
        resetGame();
    }

    public enum CELL {
        EMPTY, P1, P2
    }

    private CELL[] cell = new CELL[9];

    public boolean addToken(int player, int cell_index) {
        boolean ret_val = false;
        if ((cell_index >= 0) && (cell_index <= 8)) {
            if (cell[cell_index] == CELL.EMPTY) {
                if (player == 1) {
                    cell[cell_index] = CELL.P1;
                    ret_val = true;
                } else if (player == 2) {
                    cell[cell_index] = CELL.P2;
                    ret_val = true;
                }
            }
        }
        ledMatrix.DrawGameBoard(cell);
        return ret_val;
    }

    public boolean checkWin(int player) {
        CELL token = CELL.EMPTY;
        if (player == 1)
            token = CELL.P1;
        if (player == 2)
            token = CELL.P2;

        if (((cell[0] == token) && (cell[1] == token) && (cell[2] == token)) || // rows
                ((cell[3] == token) && (cell[4] == token) && (cell[5] == token)) ||
                ((cell[6] == token) && (cell[7] == token) && (cell[8] == token)) ||
                ((cell[0] == token) && (cell[3] == token) && (cell[6] == token)) || // columns
                ((cell[1] == token) && (cell[4] == token) && (cell[7] == token)) ||
                ((cell[2] == token) && (cell[5] == token) && (cell[8] == token)) ||
                ((cell[0] == token) && (cell[4] == token) && (cell[8] == token)) || // diagonals
                ((cell[2] == token) && (cell[4] == token) && (cell[6] == token))) {
            return true;
        }

        return false;
    }

    public boolean checkFull() {
        for (int cell_index = 0; cell_index < cell.length; cell_index++) {
            if (cell[cell_index] == CELL.EMPTY) {
                return false;
            }
        }

        return true;
    }

    public void resetGame() {
        for (int cell_index = 0; cell_index < cell.length; cell_index++) {
            cell[cell_index] = CELL.EMPTY;
        }
        ledMatrix.DrawGameBoard(cell);
    }

    public CELL[] getGrid() {
        return cell;
    }
}